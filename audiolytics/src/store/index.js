import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)




export default new Vuex.Store({
  state: {
    dialog: false,
    dialog_signin: false,
  },
  mutations: {
    dialogChange(state, new_dialog_state) {
      console.log("Changing State of the Sign Up page")
      state.dialog = new_dialog_state
    },
    dialogSignInChange(state, new_dialog_signin_state) {
      console.log("Changing State of the Signn In Page")
      state.dialog_signin = new_dialog_signin_state
    }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    dialogShowing(state){
      console.log("Gettin State of the Sign Up Page")
      return state.dialog
    },

    dialog_signinShowing(state) {
      console.log("Getting State of the Sign In Page")
      return state.dialog_signin
    }
  }
})
