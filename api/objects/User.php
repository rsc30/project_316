<?php
header("Access-Control-Allow-Origin: *");
include_once '/var/www/html/api/config/Database.php';
include_once '/var/www/html/api/objects/User.php';


class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "Track";
    private $paramName;
 
    // object properties
    private $userID;
    private $username;
    private $email;
    private $password;

    //parameter properties
    private $paramUsername;
    private $paramPassword;
    private $paramEmail;
 
    // constructor with $db as database connection
    public function __construct($db, $username, $password, $email){
        $this->conn = $db;
        $this->paramUsername = $username;
        $this->paramPassword = $password;
        $this->paramEmail = $email;
    }

    // read products
    function read(){
    
        // select all query
        $name = $this->paramName;
        $query = "SELECT * FROM User where username = '$username';";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;    
    }   

    function attempt_to_insert_user(){
        
        $name = $this->paramUsername;
        $email = $this->paramEmail;
        $password = $this->paramPassword;
        //find the user
        $query = "select * from User where username = '$name' or email = '$email';";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $user_arr = array();
        $user_arr['records'] = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
     
            $user_item=array(
                "userID" => $trackID,
                "username" => $username,
                "password" => $password,
                "email" => $email
            );
     
            array_push($user_arr['records'], $user_item);
        }

        if(count($user_arr['records'])!=0){
            return "Username or Email already exists";
        }

        $query = "insert into User (username, password, email) values ('$name','$password','$email')";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return "User was inserted into database";  

    }

    function validate_user_credentials(){
        $username = $this->paramUsername;
        $email = $this->paramEmail;
        $password = $this->paramPassword;

        $query = "select password from User where username = '$username' ;";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $user_arr = array();
        $user_arr['records'] = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
     
            $user_item=array(
                "password" => $password,
            );
     
            array_push($user_arr['records'], $user_item);
        }

        if(array_pop(array_reverse($user_arr['records']))['password']   == $this->paramPassword){
            return "User is authenticated";
        }
        return "Username or password is incorrect";

    }
}
?>