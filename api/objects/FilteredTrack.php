<?php
 

class FilteredTrack{
 
    // database connection and table name
    private $conn;
    private $table_name = "Track";
    private $paramName;
 
    // object properties
    public $trackID;
    public $name;
    public $date_released;
    public $liveness;
    public $tempo;
    public $valence;
    public $instrumentalness;
    public $acousticeness;
    public $speechiness;
    public $danceability;
    public $energy;
    public $loudness;
    public $spotifyID;
 
    // constructor with $db as database connection
    public function __construct($db, $name){
        $this->conn = $db;
        $this->paramName = $name;
    }

    // read products
    function read(){
    
        // select all query
        $name = $this->paramName;
        $query = "SELECT * FROM Track where name like '$name%'";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;    
    }   
}
?>