<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$username = $_GET["username"];
$password = $_GET["password"];
$email = $_GET["email"];


// include database and object files
include_once '/var/www/html/api/config/Database.php';
include_once '/var/www/html/api/objects/User.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$user = new User($db, $username, $password, $email);

//attempting to insert a user
if((!is_null($username)) && (!is_null($password)) && (!is_null($email))){
    $result = $user->attempt_to_insert_user();
    if($result == "Username or Email already exists"){
        $toRespond = array();
        $toRespond['user-was-created'] = FALSE;
        $toRespond['message'] = $result;
        http_response_code(404);
    }else{
        $toRespond = array();
        $toRespond['user-was-created'] = True;
        $toRespond['message'] = $result;
        http_response_code(200);
    }
    echo json_encode($toRespond);
//attempting to authenticate a user
}else if((!is_null($username)) && (!is_null($password)) && (is_null($email))){
    $result = $user->validate_user_credentials();
    if($result == "User is authenticated"){
        $toRespond = array();
        $toRespond['user-was-authenticated'] = TRUE;
        $toRespond['message'] = $result;
        http_response_code(200);
    }else{
        $toRespond = array();
        $toRespond['user-was-authenticated'] = False;
        $toRespond['message'] = $result;
        http_response_code(404);
    }
    echo json_encode($toRespond);
}





