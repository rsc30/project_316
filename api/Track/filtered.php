<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//params
$artistName = $_GET["artistName"];
$albumName = $_GET["albumName"];
$dateReleased = json_decode($_GET["dateReleased"], true);
$liveness = json_decode($_GET["liveness"], true);
$tempo = json_decode($_GET["tempo"], true);
$valence = json_decode($_GET["valence"], true);
$instrumentalness = json_decode($_GET["instrumentalness"], true);
$acousticeness = json_decode($_GET["acousticeness"], true);
$speechiness = json_decode($_GET["speechiness"], true);
$danceability = json_decode($_GET["danceability"], true);
$energy = json_decode($_GET["energy"], true);
$loudness = json_decode($_GET["loudness"], true);

// include database and object files
include_once '/var/www/html/api/config/Database.php';
include_once '/var/www/html/api/objects/Track.php';
include_once '/var/www/html/api/objects/FilteredTrack.php';

 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$query = "CREATE TEMPORARY TABLE allMatchingIDs SELECT trackID FROM Track WHERE false;";
$stmt = $db->prepare($query);
$stmt->execute();


$firstFilterHit = false;
 
//filter by artist name
//if($artistName != null or artistName){
    $query = "CREATE TEMPORARY TABLE matchingArtistIDs SELECT artistID FROM Artist WHERE name LIKE '$artistName%';";
    $stmt = $db->prepare($query);
    $stmt->execute();

    $query = "CREATE TEMPORARY TABLE matchingArtistTrackIDs SELECT trackID FROM ArtistOnTrack WHERE artistID in ( select * from matchingArtistIDs);";
    $stmt = $db->prepare($query);
    $stmt->execute();

    $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingArtistTrackIDs;";

    $stmt = $db->prepare($query);
    $stmt->execute();

    $firstFilterHit =  true;
//}

//filter by album name
if($albumName != null){
    $query = "CREATE TEMPORARY TABLE matchingAlbumIDs SELECT albumID FROM Album WHERE name LIKE '$albumName%';";
    $stmt = $db->prepare($query);
    $stmt->execute();

    $query = "CREATE TEMPORARY TABLE matchingAlbumTrackIDs SELECT trackID FROM TrackOnAlbum WHERE albumID in ( select * from matchingAlbumIDs);";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingAlbumTrackIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingAlbumTrackIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by dateReleased
if($dateReleased != "null"){
    $startDate = $dateReleased['startDate'];
    $endDate = $dateReleased['endDate'];
    $query = "CREATE TEMPORARY TABLE matchingDateIDs SELECT trackID FROM Track WHERE date_released BETWEEN DATE('$startDate') AND DATE('$endDate');";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingDateIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingDateIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by liveness
if($liveness != null){
    $minimum = $liveness['minimum'];
    $maximum = $liveness['maximum'];
    $query = "CREATE TEMPORARY TABLE matchinglivenessIDs SELECT trackID FROM Track WHERE liveness BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchinglivenessIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchinglivenessIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by tempo
if($tempo != null){
    $minimum = $tempo['minimum'];
    $maximum = $tempo['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingTempoIDs SELECT trackID FROM Track WHERE tempo BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingTempoIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingTempoIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by valence
if($valence != null){
    $minimum = $valence['minimum'];
    $maximum = $valence['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingValenceIDs SELECT trackID FROM Track WHERE valence BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingValenceIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingValenceIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by instrumentalness
if($instrumentalness != null){
    $minimum = $instrumentalness['minimum'];
    $maximum = $instrumentalness['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingInstrumentalnessIDs SELECT trackID FROM Track WHERE instrumentalness BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingInstrumentalnessIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingInstrumentalnessIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by acousticeness
if($acousticeness != null){
    $minimum = $acousticeness['minimum'];
    $maximum = $acousticeness['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingAcousticenessIDs SELECT trackID FROM Track WHERE acousticeness BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingAcousticenessIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingAcousticenessIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by speechiness
if($speechiness != null){
    $minimum = $speechiness['minimum'];
    $maximum = $speechiness['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingSpeechinessIDs SELECT trackID FROM Track WHERE speechiness BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingSpeechinessIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingSpeechinessIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by danceability
if($danceability != null){
    $minimum = $danceability['minimum'];
    $maximum = $danceability['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingDanceabilityIDs SELECT trackID FROM Track WHERE danceability BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingDanceabilityIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingDanceabilityIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by energy
if($energy != null){
    $minimum = $energy['minimum'];
    $maximum = $energy['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingEnergyIDs SELECT trackID FROM Track WHERE energy BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingEnergyIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingEnergyIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}

//filter by loudness
if($loudness != null){
    $minimum = $loudness['minimum'];
    $maximum = $loudness['maximum'];
    $query = "CREATE TEMPORARY TABLE matchingLoudnessIDs SELECT trackID FROM Track WHERE loudness BETWEEN $minimum AND $maximum;";
    $stmt = $db->prepare($query);
    $stmt->execute();

    if($firstFilterHit){
        $query = "DELETE FROM allMatchingIDs WHERE trackID NOT IN (SELECT * FROM matchingLoudnessIDs)";
    }else{
        $query = "INSERT INTO allMatchingIDs SELECT trackID FROM matchingLoudnessIDs;";
        $firstFilterHit =  true;
    }
    
    $stmt = $db->prepare($query);
    $stmt->execute();
}



$query = "SELECT * FROM Track where trackID in (select * from allMatchingIDs);";
$stmt = $db->prepare($query);
$stmt->execute();

//delete DUPLICATES
 
// check if more than 0 record found
// products array
$products_arr=array();
$products_arr["records"]=array();

//generates headers
$products_arr["headers"]=array();
array_push($products_arr["headers"], array('text' => 'Track ID', 'value' => 'trackID'));
array_push($products_arr["headers"], array('text' => 'Title', 'value' => 'name'));
array_push($products_arr["headers"],array('text' => 'Date Released','value' => 'date_released'));
array_push($products_arr["headers"],array('text' => 'Liveness','value' => 'liveness'));
array_push($products_arr["headers"], array('text' => 'Liveness', 'value' => 'liveness'));
array_push($products_arr["headers"], array('text' => 'Tempo','value' => 'tempo'));
array_push($products_arr["headers"], array('text' => 'Valence', 'value' => 'valence'));
array_push($products_arr["headers"], array('text' => 'Instrumentalness', 'value' => 'instrumentalness'));
array_push($products_arr["headers"], array('text' => 'Acousticeness','value' => 'acousticeness'));
array_push($products_arr["headers"], array('text' => 'Speechiness','value' => 'speechiness'));
array_push($products_arr["headers"], array('text' => 'Danceability', 'value' => 'danceability'));
array_push($products_arr["headers"], array('text' => 'Energy', 'value' => 'energy'));
array_push($products_arr["headers"], array('text' => 'Loudness','value' => 'loudness'));
//array_push($products_arr["headers"], array('text' => 'spotifyID','value' => 'SpotifyID'));

    
// retrieve our table contents
// fetch() is faster than fetchAll()
// http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    // extract row
    // this will make $row['name'] to
    // just $name only
    extract($row);

    $product_item=array(
        "trackID" => $trackID,
        "name" => $name,
        "date_released" => $date_released,
        "liveness" => $liveness,
        "tempo" => $tempo,
        "valence" => $valence,
        "instrumentalness" => $instrumentalness,
        "acousticeness" => $acousticeness,
        "speechiness" => $speechiness,
        "danceability" => $danceability,
        "energy" => $energy,
        "loudness" => $loudness,
        "spotifyID" => $spotifyID,
    );

    array_push($products_arr["records"], $product_item);
}

// set response code - 200 OK
http_response_code(200);

// show products data in json format
echo json_encode($products_arr);
