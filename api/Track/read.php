<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$name = $_GET["name"];

// include database and object files
include_once '/var/www/html/api/config/Database.php';
include_once '/var/www/html/api/objects/Track.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$track = new Track($db, $name);

// query products
$stmt = $track->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $products_arr=array();
    $products_arr["records"]=array();
    $products_arr["headers"]=array();
    array_push($products_arr["headers"], array('text' => 'Track ID', 'value' => 'trackID'));
    array_push($products_arr["headers"], array('text' => 'Title', 'value' => 'name'));
    array_push($products_arr["headers"],array('text' => 'Date Released','value' => 'date_released'));
    array_push($products_arr["headers"],array('text' => 'Liveness','value' => 'liveness'));
    array_push($products_arr["headers"], array('text' => 'Tempo','value' => 'tempo'));
    array_push($products_arr["headers"], array('text' => 'Valence', 'value' => 'valence'));
    array_push($products_arr["headers"], array('text' => 'Instrumentalness', 'value' => 'instrumentalness'));
    array_push($products_arr["headers"], array('text' => 'Acousticeness','value' => 'acousticeness'));
    array_push($products_arr["headers"], array('text' => 'Speechiness','value' => 'speechiness'));
    array_push($products_arr["headers"], array('text' => 'Danceability', 'value' => 'danceability'));
    array_push($products_arr["headers"], array('text' => 'Energy', 'value' => 'energy'));
    array_push($products_arr["headers"], array('text' => 'Loudness','value' => 'loudness'));
    //array_push($products_arr["headers"], array('text' => 'spotifyID','value' => 'SpotifyID'));

        
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "trackID" => $trackID,
            "name" => $name,
            "date_released" => $date_released,
            "liveness" => $liveness,
            "tempo" => $tempo,
            "valence" => $valence,
            "instrumentalness" => $instrumentalness,
            "acousticeness" => $acousticeness,
            "speechiness" => $speechiness,
            "danceability" => $danceability,
            "energy" => $energy,
            "loudness" => $loudness,
            "spotifyID" => $spotifyID,
        );
 
        array_push($products_arr["records"], $product_item);
    }
 
    // set response code - 200 OK
    // http_response_code(200);
 
    // show products data in json format
    echo json_encode($products_arr);
}
 
// no products found will be here

else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No products found.")
    );
}