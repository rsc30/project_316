# How to use the back-end API

## Users

endpoint = http://34.69.198.71/api/User/read.php

### Authenticating users

#### Parameters
This requires the following parameters
```json
{
    "username":   String,
    "password":   String
}
```

#### Response
```json
{
    "user-was-authenticated":   boolean,
    "message":                  String
}
```

### Creating Users

#### Parameters
This requires the following parameters
```json
{
    "username":   String,
    "password":   String,
    "email":      String
}
```

#### Response

This requires the following parameters
```json
{
    "user-was-created": boolean,
    "message":          String
}
```



## Filtering Lists

Endpoint http://34.69.198.71/api/Track/filtered.php

#### Parameters

**Note:** all parameters are optional, meaning if you leave them blank, the API wont filter by those parameters.

```json
{
    "artistName": String,
    "albumName": String,
    "genre": String,
    "date_releases":{
        "startDate": String,
        "endDate" : String
    },
    "liveness":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"tempo":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 300)
    },"valence":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"instrumentalness":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"acousticeness":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"speechiness":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"danceability":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"energy":{
        "minimum": float (minimum 0),
        "maximum": float (maximum) 1)
    },"loudness":{
        "minimum": float (minimum -60),
        "maximum": float (maximum) 0)
    }
}
```

#### Reponse

```json
{
    "records":[
        {track},
        {track},
        ...
    ],
    "headers":[
        {header},
        {header},
        ...
    ]
}
```

