import requests

"""class used to help with spotify queries"""
class SpotifyPayloadGenerator:


    QUERY = "q"
    TYPE = "type"
    LIMIT = "limit"

    ARTIST = "artist"

    AUHTORIZATION = "Authorization"
    BEARER = "Bearer"

    """artist name to ID"""
    def generate_artist_from_name_payload(self, name):
        return {
            self.QUERY: name,
            self.TYPE:  self.ARTIST,
            self.LIMIT: 1
        }

    """generates the payload for getting the token"""
    def generate_get_token_payload(self):
        return {
            "grant_type":"client_credentials"
        }

    """generates the auth payload"""
    def generate_get_token_auth_payload(self, clientID, secretID):
        return requests.auth.HTTPBasicAuth(clientID, secretID)

    """generates the paylaod for the bearer token auth"""
    def generate_bearer_token_payload(self, token):
        return {
            self.AUHTORIZATION: "{bearer} {theToken}".format(bearer=self.BEARER, theToken=token)
        }
        

    