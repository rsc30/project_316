import ConfigReader
import requests
import base64
import SpotifyPayLoadGenerator
from spotifyRows import Artist
from spotifyRows import Album
from spotifyRows import Track
import json
import time

class SpotifyConnector:

    NUMBER_OF_ALBUMS_TO_RETURN = 10

    SPOTIFY_AUTH_ENDPOINT = "https://accounts.spotify.com/api/token"
    #json keys
    ACCESS_TOKEN_TEXT = "access_token"
    ACCESS_TOKEN_TYPE_TEXT = "token_type"


    SPOTIFY_SEARCH_ENDPOINT = "https://api.spotify.com/v1/search"
    #json keys
    SEARCH_ARTIST = "artists"
    SEARCH_ITEMS = "items"
    SEARCH_NAME = "name"
    SEARCH_ARTIST_ID = "id"

    SPOTIFY_ARTIST_ALBUM_ENDPOINT = "https://api.spotify.com/v1/artists/{id}/albums"

    SPOTIFY_ALBUM_ENDPOINT = "https://api.spotify.com/v1/albums/{id}"

    SPOTIFY_ALBUM_TRACKS_ENDPOINT = "https://api.spotify.com/v1/albums/{id}/tracks"

    SPOTIFY_GET_TRACK_ENDPOINT = "https://api.spotify.com/v1/tracks/{id}"

    SPOTIFY_AUDIO_FEATURES_ENDPOINT = "https://api.spotify.com/v1/audio-features/{id}"


    config = ConfigReader.ConfigReader()

    access_token = ""
    access_token_type = ""

    payloadGenerator = SpotifyPayLoadGenerator.SpotifyPayloadGenerator()

    """Connects the application to the spotify API """
    def __init__(self):
        self.get_new_access_token()

    def is_request_valid(self, request):
        check = request.json()
        if('error' in check.keys()):
            print(check['error']['message'])
            print("Waiting 10 secs")
            time.sleep(10)
            print("DONE!")
            return False
        return True

    """Gets new access token"""
    def get_new_access_token(self):
        r = requests.post(  
            url = self.SPOTIFY_AUTH_ENDPOINT, 
            auth=self.payloadGenerator.generate_get_token_auth_payload(self.config.getClientID(), self.config.getClientSecret()), 
            data =self.payloadGenerator.generate_get_token_payload())            
        self.access_token = r.json()[self.ACCESS_TOKEN_TEXT]
        self.access_token_type = r.json()[self.ACCESS_TOKEN_TYPE_TEXT]

    """attempt to get ID based on name"""
    def get_spotify_artistID_from_name(self, name):
        r = requests.get(
            url = self.SPOTIFY_SEARCH_ENDPOINT,
            params= self.payloadGenerator.generate_artist_from_name_payload(name),
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token),
        )
        if not self.is_request_valid(r):
            return self.get_spotify_artistID_from_name(name)
        return self.return_artist_id_from_response(r, name)

    """returns the id of an artist if the name matches exactly
        Otherwise it returns None"""
    def return_artist_id_from_response(self, result, name):
        resultMap = result.json()[self.SEARCH_ARTIST][self.SEARCH_ITEMS][0]
        toValidate = resultMap[self.SEARCH_NAME]
        #ASSUMES THERE WILL BE ONE ITEM IN THE LIST
        if(toValidate == name):
            return Artist.Artist(resultMap)
        else:
            return None
        
    def get_albums_from_spotify_artist(self, spotifyArtist):
        r = requests.get(
            url= self.SPOTIFY_ARTIST_ALBUM_ENDPOINT.format(id=spotifyArtist.get_spotify_id()),
            params={'limit':50},
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token)
        )
        if not self.is_request_valid(r):
            return self.get_albums_from_spotify_artist(spotifyArtist)
        return self.return_list_albums_from_response(r)

    def return_list_albums_from_response(self, r):
        resultList= self.validate_album_artist_data(r.json()['items'])
        return resultList

    def validate_album_artist_data(self, resultList):
        #remove all singles
        resultList = [i for i in resultList if i['album_type'] != "single"]
        #remove duplicate album names
        albumNamesStored = []
        uniqueAlbums = []
        for result in resultList:
            if(result['name'] not in albumNamesStored):
                albumNamesStored.append(result['name'])
                uniqueAlbums.append(result)
        return [i['id'] for i in uniqueAlbums][:self.NUMBER_OF_ALBUMS_TO_RETURN]

    def get_album_from_spotify_album_id(self, spotifyAlbumID):
        r = requests.get(
            url=self.SPOTIFY_ALBUM_ENDPOINT.format(id=spotifyAlbumID),
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token)
        )
        if not self.is_request_valid(r):
            return self.get_album_from_spotify_album_id(spotifyAlbumID)
        return Album.Album(r.json())

    def get_tracks_from_spotify_album_id(self, spotifyAlbumID):
        r =requests.get(
            url=self.SPOTIFY_ALBUM_TRACKS_ENDPOINT.format(id=spotifyAlbumID),
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token)
        )
        if not self.is_request_valid(r):
            return self.get_tracks_from_spotify_album_id(spotifyAlbumID)
        return [i['id'] for i in r.json()['items']]

    def get_track_from_spotifyID(self, spotifyID):
        trackInfoRequest = requests.get(
            url=self.SPOTIFY_GET_TRACK_ENDPOINT.format(id=spotifyID),
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token)
        )
        audioFeaturesRequest = requests.get(
            url=self.SPOTIFY_AUDIO_FEATURES_ENDPOINT.format(id=spotifyID),
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token)
        )
        if not self.is_request_valid(trackInfoRequest) or not self.is_request_valid(audioFeaturesRequest) :
            return self.get_track_from_spotifyID(spotifyID)

        return Track.Track(trackInfoRequest.json(), audioFeaturesRequest.json())

    def get_spotify_artist_from_id(self, artistSpotifyID):
        r = requests.get(
            url="https://api.spotify.com/v1/artists/{id}".format(id=artistSpotifyID),
            headers=self.payloadGenerator.generate_bearer_token_payload(self.access_token)
        )
        if not self.is_request_valid(r):
            return self.get_spotify_artist_from_id(artistSpotifyID)
        return Artist.Artist(r.json())
