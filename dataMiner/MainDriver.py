import SpotifyConnector
import DatabaseConnector
from dataToGet import ArtistNameLoader


"""Driver responsible for getting the data for the database"""
class MainDriver:

    spotifyConnector = None
    databaseConnector = None
    artistNameLoader = None

    NUM_ALBUMS_TO_GET = 20

    """Initializes the MainDriver"""
    def __init__(self):
        self.spotifyConnector = SpotifyConnector.SpotifyConnector()
        self.databaseConnector = DatabaseConnector.DatabaseConnector()
        self.artistLoader = ArtistNameLoader.ArtistNameLoader()

    def add_artists_indefinitely(self):
        while True:
            self.attempt_to_add_artist(self.artistLoader.getNextArtist())
        

    """Adds a number of new artists to the database, including their albums and songs"""
    def add_new_artists(self, numberOfArtistsToAdd):
        for i in range(numberOfArtistsToAdd):
            self.attempt_to_add_artist(self.artistLoader.getNextArtist())

    """attempts to add an artist to the database"""
    def attempt_to_add_artist(self, artistName):
        spotifyArtist = self.spotifyConnector.get_spotify_artistID_from_name(artistName)
        if(spotifyArtist is not None):
            if(not self.databaseConnector.does_artist_exist_in_database(spotifyArtist)):
                self.insert_artist_into_database(spotifyArtist)
            artistID = self.databaseConnector.get_artist_from_name(spotifyArtist)
            self.insert_artist_associated_genre(spotifyArtist, artistID)
            self.add_current_artist_albums(spotifyArtist)

    """inserts a valid artist into the database"""
    def insert_artist_into_database(self, spotifyArtist):
        self.databaseConnector.insert_artist_into_database(spotifyArtist)

    def insert_artist_associated_genre(self, spotifyArtist, currentArtistID):
        for genre in spotifyArtist.get_genres():
            self.databaseConnector.insert_associated_genre(genre, currentArtistID, "artist")
    
    def add_current_artist_albums(self, spotifyArtist):
        artistID = self.databaseConnector.get_artist_from_name(spotifyArtist)
        artistAlbumIDs = self.spotifyConnector.get_albums_from_spotify_artist(spotifyArtist)
        for spotifyAlbumID in artistAlbumIDs:
            self.attempt_to_add_album(spotifyAlbumID, artistID)
    
    def attempt_to_add_album(self, spotifyAlbumID, artistID):
        spotifyAlbum = self.spotifyConnector.get_album_from_spotify_album_id(spotifyAlbumID)
        self.attempt_to_insert_album(spotifyAlbum, artistID)

    def attempt_to_insert_album(self, spotifyAlbum, artistID):
        if(not self.databaseConnector.does_album_exist_in_database(spotifyAlbum.get_album_spotify_id())):
            self.databaseConnector.insert_album_into_database(spotifyAlbum)
        albumID = self.databaseConnector.get_album_id_from_spotifyID(spotifyAlbum.get_album_spotify_id())
        if(not self.databaseConnector.is_artist_associated_to_album(albumID, artistID)):
            self.databaseConnector.insert_association_between_artist_and_album(albumID, artistID)
        self.add_current_albums_tracks(albumID, spotifyAlbum, artistID)
    
    def add_current_albums_tracks(self, albumID, spotifyAlbum, artistID):
        tracksOnAlbum = self.spotifyConnector.get_tracks_from_spotify_album_id(spotifyAlbum.get_album_spotify_id())
        for spotifyTrackID in tracksOnAlbum:
            self.attempt_to_add_track(spotifyTrackID, albumID)

    def attempt_to_add_track(self, spotifyTrackID, albumID):
        if(not self.databaseConnector.does_track_exist_in_database(spotifyTrackID)):
            track = self.spotifyConnector.get_track_from_spotifyID(spotifyTrackID)
            self.databaseConnector.insert_track_into_database(track)
        else:
            track = self.spotifyConnector.get_track_from_spotifyID(spotifyTrackID)
        self.attempt_to_associate_track_to_album(track, albumID)
        self.associate_track_to_its_artists(track)

    def attempt_to_associate_track_to_album(self, track, albumID):
        trackID = self.databaseConnector.get_track_id_from_spotify_id(track.get_spotifyID())
        if(not self.databaseConnector.is_track_associated_to_album(trackID, albumID)):
             self.databaseConnector.associate_track_and_album(trackID, albumID)

    def associate_track_to_its_artists(self, track):
       for artistSpotifyID in [i['id'] for i in track.get_artists()]:
           self.attempt_to_associate_track_and_artist(track,artistSpotifyID)

    def attempt_to_associate_track_and_artist(self, track, artistSpotifyID):
        if(not self.databaseConnector.does_artist_exist_from_spotify_ID(artistSpotifyID)):
            self.insert_artist_using_spotifyID(artistSpotifyID)
        trackID = self.databaseConnector.get_track_id_from_spotify_id(track.get_spotifyID())
        artistID = self.databaseConnector.get_artistID_from_spotifyID(artistSpotifyID)
        if(not self.databaseConnector.are_artist_and_track_associated(trackID,artistID)):
            self.databaseConnector.associate_track_and_artist(trackID,artistID)        

    def insert_artist_using_spotifyID(self, artistSpotifyID):
        artist = self.spotifyConnector.get_spotify_artist_from_id(artistSpotifyID)
        self.databaseConnector.insert_artist_into_database(artist)