"""class that represents spotify artist"""

class Artist:

    attributes = {}

    ARTIST_NAME = "name"
    ARTIST_ID = "id"
    ARTIST_FOLLOWERS = "followers"
    ARTIST_POPULARITY = "popularity"


    def __init__(self, attributeMap):
        self.attributes = attributeMap
    
    def get_name(self):
        return self.attributes[self.ARTIST_NAME]
    
    def get_spotify_id(self):
        return self.attributes[self.ARTIST_ID]
    
    def get_spotify_followers(self):
        return self.attributes[self.ARTIST_FOLLOWERS]["total"]
    
    def get_spotify_popularity(self):
        return self.attributes[self.ARTIST_POPULARITY]
    
    def get_attributes(self):
        return self.attributes

    def get_genres(self):
        return self.attributes["genres"]