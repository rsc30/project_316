class Album:
    attributes = {}

    def __init__(self, attributeMap):
        self.attributes = attributeMap

    def get_album_name(self):
        return self.attributes["name"]

    def get_album_spotify_id(self):
        return self.attributes["id"]

    def get_album_popularity(self):
        return self.attributes["popularity"]
    
    def get_album_release_date(self):
        return self.attributes["release_date"]

    def get_album_type(self):
        return self.attributes["type"]

    def get_album_genres(self):
        return self.attributes['genres']