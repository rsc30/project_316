class Track:

    trackInfo = {}
    audioFeatures = {}

    def __init__(self, trackInfo, audioFeatures):
        self.trackInfo = trackInfo
        self.audioFeatures = audioFeatures

    def get_name(self):
        return self.trackInfo['name']

    def get_date_released(self):
        return self.trackInfo['album']['release_date']

    def get_liveness(self):
        return self.audioFeatures['liveness']

    def get_valence(self):
        return self.audioFeatures['valence']

    def get_instrumentalness(self):
        return self.audioFeatures['instrumentalness']

    def get_acousticeness(self):
        return self.audioFeatures['acousticness']

    def get_speechiness(self):
        return self.audioFeatures['speechiness']

    def get_danceability(self):
        return self.audioFeatures['danceability']

    def get_loudness(self):
        return self.audioFeatures['loudness']

    def get_spotifyID(self):
        return self.trackInfo['id']

    def get_tempo(self):
        return self.audioFeatures['tempo']

    def get_energy(self):
        return self.audioFeatures['energy']
    
    def get_album_spotify_id(self):
        return self.trackInfo['album']['id']

    def get_artists(self):
        return self.trackInfo['artists']