from spotifyRows import Artist
from spotifyRows import Album

"""Class used to standardize sql calls"""
class SQLCalls:

    ARTIST_TABLE = "Artist"
    ARTIST_NAME_COLUMN = "name"
    ARTIST_ID_COLUMN = "artistID"
    ARTIST_SPOTIFY_ID_COLUMN = "spotifyID"
    ARIST_SPOTIFY_FOLLOWERS = "followers"
    ARIST_SPOTIFY_POPULARITY = "popularity"

    def generate_select_from_artist(self, spotifyArtist):
        sql = "SELECT * FROM {artist_table} WHERE {artist_name} = \"{the_name}\";".format(
            artist_table=self.ARTIST_TABLE,
            artist_name=self.ARTIST_NAME_COLUMN,
            the_name=spotifyArtist.get_name()
        )
        return sql

    def generate_insert_artist_query(self, spotifyArtist):
        sql = """   INSERT INTO {artistTable} ({nameColumn},{spotifyIDColumn},{popularityCol},{followerCol}) 
                    VALUES (\"{artistName}\",\"{spotifyID}\",{popularity},{follower});
                """.format(
                    artistTable=self.ARTIST_TABLE,
                    nameColumn=self.ARTIST_NAME_COLUMN,
                    popularityCol=self.ARIST_SPOTIFY_POPULARITY,
                    followerCol=self.ARIST_SPOTIFY_FOLLOWERS,
                    spotifyIDColumn=self.ARTIST_SPOTIFY_ID_COLUMN,
                    artistName=spotifyArtist.get_name(),
                    spotifyID=spotifyArtist.get_spotify_id(),
                    popularity= spotifyArtist.get_spotify_popularity(),
                    follower=spotifyArtist.get_spotify_followers()
        )
        return sql

    def select_genre_by_name(self, genre):
        sql = "SELECT * from Genre WHERE genre = '{theGenre}';".format(
            theGenre=genre
        )
        return sql

    def generate_insert_into_genre(self, genre):
        sql = "INSERT INTO Genre (genre) VALUES ('{theGenre}')".format(
            theGenre=genre
        )
        return sql

    def generate_select_genre_id_from_genre_name(self, genre):
        sql = "SELECT genreID FROM Genre WHERE genre = '{theGenre}';".format(
            theGenre=genre
        )
        return sql

    def generate_select_from_associated_genre_using_all(self, genreID, id, tableType):
        sql = "Select * from AssociatedGenre where genreID = {genreID} AND associateID = {id} AND associateIDType = '{tableType}'".format(
            genreID=genreID,
            id=id,
            tableType=tableType
        )
        return sql

    def generate_insert_genre_associate(self, genreID, id, typeOfTable):
        sql =   """ INSERT INTO AssociatedGenre (genreID, associateID, associateIDType) 
                    VALUES ('{theGenreID}','{theAssociateID}','{theAssociateIDType}');
                """.format(
                    theGenreID=genreID,
                    theAssociateID = id,
                    theAssociateIDType = typeOfTable
                )
        return sql

    def genereate_select_album_with_spotifyID(self, spotifyID):
        sql = "SELECT albumID from Album where spotifyID = '{spotifyID}';".format(spotifyID=spotifyID)
        return sql

    def genereate_select_association_with_albumID_artistID(self, albumID, artistID):
        sql = "Select * from ArtistOnAlbum where albumID = {albumID} and artistID = {artistID}".format(
            albumID=albumID,
            artistID=artistID
        )
        return sql

    def generate_insert_album_query(self, spotifyAlbum):

        date_released = spotifyAlbum.get_album_release_date()
        if "-" not in date_released:
            date_released = date_released + "-1-1"

        sql =   """ Insert into Album (name, date_released, popularity, albumType, spotifyID) values 
                    (\"{name}\", '{date_released}',{popularity},'{albumType}','{spotifyID}');
                """.format(
                    name=spotifyAlbum.get_album_name()[:100],
                    date_released=date_released,
                    popularity=spotifyAlbum.get_album_popularity(),
                    albumType=spotifyAlbum.get_album_type(),
                    spotifyID=spotifyAlbum.get_album_spotify_id()
                ) 
        return sql

    def generate_insert_album_artist_association(self, albumID, artistID):
        sql =   "Insert into ArtistOnAlbum (artistID, albumID) values ({artistID}, {albumID});".format(
            albumID=albumID,
            artistID=artistID
        )
        return sql     

    def generate_select_from_track_by_spotifyID(self, spotifyID):
        sql = "Select * from Track where spotifyID = '{spotifyID}'".format(spotifyID=spotifyID)
        return sql

    def generate_insert_track(self, track):
        date_released=track.get_date_released()
        if "-" not in date_released:
            date_released = date_released + "-1-1"
        sql =   """ Insert into Track (name, date_released, liveness, tempo, valence, instrumentalness, acousticeness, speechiness, danceability, energy, loudness, spotifyID) values
                    (\"{name}\", '{date_released}', {liveness}, {tempo}, {valence}, {instrumentalness}, {acousticeness}, {speechiness}, {danceability}, {energy}, {loudness}, '{spotifyID}' )
                """.format(
                    name=track.get_name().replace("\"","")[:100], 
                    date_released=date_released, 
                    liveness=track.get_liveness(), 
                    tempo=track.get_tempo(), 
                    valence=track.get_valence(), 
                    instrumentalness=track.get_instrumentalness(), 
                    acousticeness=track.get_acousticeness(), 
                    speechiness=track.get_speechiness(), 
                    danceability=track.get_danceability(), 
                    energy=track.get_energy(), 
                    loudness=track.get_loudness(), 
                    spotifyID=track.get_spotifyID()
                )
        return sql

 