import csv

"""returns artists by their popularity"""
class ArtistNameLoader:

    ARTIST_CSV_PATH = "dataToGet/topArtists.csv"
    ARTIST_NAME_COLUMN = 0
    DELIMETER = ","

    current_row = 0
    reader = None
    csvFile = None

    """ initializes the artist loader"""
    def __init__(self):
        self.openCSVFile()

    """opens the csv file and sets the reader to the first data row"""
    def openCSVFile(self):
        csvFile = open(self.ARTIST_CSV_PATH) 
        self.reader = csv.reader(csvFile, delimiter=self.DELIMETER)
        self.getNextArtist()

    """gets the next artist's name"""
    def getNextArtist(self):
        return self.extractName(next(self.reader))

    """extracts the name from a row of the csv file"""
    def extractName(self, toExtract):
        return toExtract[self.ARTIST_NAME_COLUMN].strip()

        