import configparser

class ConfigReader:
    """Reads information from a config.ini file in same directory"""

    FILEPATH = "config.ini"
    SECTION = "DATABASE"
    USER = "user"
    PASSWORD = "password"
    HOST = "host"
    DATABASE = "database"
    SPOTIFY = "SPOTIFY"
    clientid = "clientid"
    clientsecret = "clientsecret"
    callbackuri = "callbackuri"

    def __init__(self):
        """constructor for class"""
        self.config = configparser.ConfigParser()
        self.config.read(self.FILEPATH)

    def getUser(self):
        """returns the user"""
        return self.config[self.SECTION][self.USER]

    def getPassword(self):
        """returns the password"""
        return self.config[self.SECTION][self.PASSWORD]

    def getHost(self):
        """returns the host"""
        return self.config[self.SECTION][self.HOST]

    def getDatabase(self):
        """returns the database"""
        return self.config[self.SECTION][self.DATABASE]

    def getRedirectURI(self):
        """ returns redirect uri"""
        return self.config[self.SPOTIFY][self.callbackuri]

    def getClientID(self):
        """ returns client id"""
        return self.config[self.SPOTIFY][self.clientid]

    def getClientSecret(self):
        """ returns client secret"""
        return self.config[self.SPOTIFY][self.clientsecret]


