import pymysql
import ConfigReader
import SQLCalls

from spotifyRows import Artist

class DatabaseConnector:

    connection = None
    current_cursor = None
    config = ConfigReader.ConfigReader()
    sql = SQLCalls.SQLCalls()

    #### artist columns
    ARTIST_NAME = "name"
    ARTIST_ID = "artistID"
    ARTIST_SPOTIFY_ID = "spotifyID"
    ARTIST_FOLLOWERS = "followers"
    ARTIST_POPULARITY = "popularity"
    ARTIST_COLUMNS = {
        ARTIST_ID   :       0,
        ARTIST_NAME :       1,
        ARTIST_SPOTIFY_ID:  2,
        ARTIST_FOLLOWERS:   3,
        ARTIST_POPULARITY:  4
    }

    """initializes a connection to the database"""
    def __init__(self):
        self.createAConnection()
        self.createAndSetCurrentCursor()
        self.checkConnection()

    """creates a connection to the mysql server"""
    def createAConnection(self):
        self.connection =  pymysql.connect( 
            self.config.getHost(),
            self.config.getUser(),
            self.config.getPassword(),
            self.config.getDatabase()
        )
        #return self.connection

    """create and sets a cursor"""
    def createAndSetCurrentCursor(self):
        self.current_cursor = self.connection.cursor()

    """"Checks the connections"""
    def checkConnection(self):
        self.current_cursor.execute("SELECT VERSION()")

    """executes a query and returns a list of tuples"""
    def executeQuery(self, query):
        self.current_cursor.execute(query)
        return self.current_cursor.fetchall()

    """does artist exist in database"""
    def does_artist_exist_in_database(self, spotifyArtist):
        self.current_cursor.execute(self.sql.generate_select_from_artist(spotifyArtist))
        result = self.current_cursor.fetchall()
        if(len(result) == 0):
            return False
        return True
    
    """inserts an artist into the database"""
    def insert_artist_into_database(self, spotifyArtist):
        self.current_cursor.execute(
            self.sql.generate_insert_artist_query(spotifyArtist)
        )
        self.connection.commit()
        print("Artist: {theName} successfully inserted!".format(theName=spotifyArtist.get_name()))

    def get_artist_from_name(self, spotifyArtist):
        self.current_cursor.execute(
            self.sql.generate_select_from_artist(spotifyArtist)
        )
        return self.current_cursor.fetchone()[self.ARTIST_COLUMNS[self.ARTIST_ID]]

    def insert_associated_genre(self, genre, id, typeOfTable):
        if(not self.does_genre_exist(genre)):
            self.insert_new_genre(genre)
        genreID = self.get_genre_id(genre)
        if(not self.does_genre_tuple_exist(genreID, id, typeOfTable)):
            self.current_cursor.execute(
                self.sql.generate_insert_genre_associate(genreID, id, typeOfTable)
            )
            self.connection.commit()
            print("Succesfully associated {tableType} id of {theID} to genre id of {theGenreID} ".format(
                tableType=typeOfTable,
                theID=id,
                theGenreID=genreID))

    def does_genre_tuple_exist(self, genreID, id, typeOfTable):
        self.current_cursor.execute(
            self.sql.generate_select_from_associated_genre_using_all(genreID, id, typeOfTable)
        )
        result = self.current_cursor.fetchone()
        if(result is None):
            return False
        return True
        
    def does_genre_exist(self, genre):
        self.current_cursor.execute(
            self.sql.select_genre_by_name(genre)
        )
        result = self.current_cursor.fetchone()
        if(result is None):
            return False
        return True

    def insert_new_genre(self, genre):
        self.current_cursor.execute(
            self.sql.generate_insert_into_genre(genre)
        )
        self.connection.commit()
        print("Succesfully inserted genre: " + genre )

    def get_genre_id(self, genre):
        self.current_cursor.execute(
            self.sql.generate_select_genre_id_from_genre_name(genre)
        )
        return self.current_cursor.fetchone()[0]

    def does_album_exist_in_database(self, spotifyID):
        self.current_cursor.execute(
            self.sql.genereate_select_album_with_spotifyID(spotifyID)
        )
        response = self.current_cursor.fetchone()
        if(response is None):
            return False
        return True

    def insert_album_into_database(self, spotifyAlbum):
        self.current_cursor.execute(
            self.sql.generate_insert_album_query(spotifyAlbum)
        )
        self.connection.commit()
        print("Succesfully inserted album: " + spotifyAlbum.get_album_name()[:100] + " into database")

    def get_album_id_from_spotifyID(self, spotifyID):
        self.current_cursor.execute(
            self.sql.genereate_select_album_with_spotifyID(spotifyID)
        )
        return self.current_cursor.fetchone()[0]

    def is_artist_associated_to_album(self, albumID, artistID):
        self.current_cursor.execute(
            self.sql.genereate_select_association_with_albumID_artistID(albumID, artistID)
        )
        response = self.current_cursor.fetchone()
        if(response is None):
            return False
        return True

    def insert_association_between_artist_and_album(self, albumID, artistID):
        self.current_cursor.execute(
            self.sql.generate_insert_album_artist_association(albumID, artistID)
        )
        self.connection.commit()
        print("Succesfully inserted association between artist: {artistID} and album: {albumID}".format(
            artistID=artistID,
            albumID=albumID
        ))

    def does_track_exist_in_database(self, spotifyTrackID):
        self.current_cursor.execute(
            self.sql.generate_select_from_track_by_spotifyID(spotifyTrackID)
        )
        response = self.current_cursor.fetchone()
        if(response is None):
            return False
        return True

    def insert_track_into_database(self, track):
        self.current_cursor.execute(
            self.sql.generate_insert_track(track)
        )
        self.connection.commit()
        print("Succesfully inserted track: " + track.get_name() + " into databse")

    def get_track_id_from_spotify_id(self, spotifyTrackID):
        self.current_cursor.execute(
            "Select trackID from Track where spotifyID = '{spotifyID}'".format(
                spotifyID = spotifyTrackID
            )
        )
        return self.current_cursor.fetchone()[0]

    def is_track_associated_to_album(self, trackID, albumID):
        self.current_cursor.execute(
            "select * from TrackOnAlbum where trackID = {trackID} and albumID = {albumID}".format(
                albumID=albumID,
                trackID=trackID
            )
        )
        response = self.current_cursor.fetchone()
        if(response is None):
            return False
        return True

    def associate_track_and_album(self, trackID, albumID):
        self.current_cursor.execute(
            "insert into TrackOnAlbum (trackID, albumID) values ({trackID}, {albumID})".format(
                albumID=albumID,
                trackID=trackID
            )
        )
        self.connection.commit()
        print("succesfully associated track: {trackID} to album: {albumID}".format(trackID=trackID, albumID=albumID) )

    def does_artist_exist_from_spotify_ID(self, artistSpotifyID):
        self.current_cursor.execute(
            "select * from Artist where spotifyID = '{artistSpotifyID}'".format(
                artistSpotifyID=artistSpotifyID
            )
        )
        response = self.current_cursor.fetchone()
        if(response is None):
            return False
        return True

    def get_artistID_from_spotifyID(self, spotifyID):
        self.current_cursor.execute(
            "select artistID from Artist where spotifyID = '{spotifyID}'".format(
                spotifyID=spotifyID
            )
        )
        return self.current_cursor.fetchone()[0]

    def are_artist_and_track_associated(self, trackID, artistID):
        self.current_cursor.execute(
            "select * from ArtistOnTrack where artistID = {artistID} and trackID = {trackID}".format(
                artistID=artistID,
                trackID=trackID
            )
        )
        response = self.current_cursor.fetchone()
        if(response is None):
            return False
        return True

    def associate_track_and_artist(self, trackID, artistID):
        self.current_cursor.execute(
            "insert into ArtistOnTrack (trackID, artistID) values ({trackID}, {artistID})".format(
                artistID=artistID,
                trackID=trackID
            )
        )
        self.connection.commit()
        print("succesfully associated track: {trackID} to artist: {artistID}".format(
                artistID=artistID,
                trackID=trackID
            ))
    
    def getConnection(self):
        return self.connection

    def getCurrentCursor(self):
        return self.current_cursor.fetchone()[0]

    

    