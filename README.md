# Where to Find Project Code Directories

## Python Data Miner (To Get Data From Spotify)

https://gitlab.oit.duke.edu/rsc30/project_316/tree/master/dataMiner

## Front End

https://gitlab.oit.duke.edu/rsc30/project_316/tree/master/audiolytics

## API

https://gitlab.oit.duke.edu/rsc30/project_316/tree/master/api